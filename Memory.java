import java.util.*;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/* package */ class Memory {
	Logger log = LogManager.getLogger(Memory.class.getName());
	private HashMap<Integer, String> hashMap = null;

	public Memory() {
		hashMap = new HashMap<Integer, String>();
	}

	public void addObject(int objId, String value) {
		log.trace("MEMORY:STORE (" + objId +"," + value+")");
		hashMap.put(objId, value);
	}

	public String getObject(int objId) {
		String value = hashMap.get(objId);
		log.trace("MEMORY:LOAD (" + objId + "," + value+")");
		return value;
	}
}
