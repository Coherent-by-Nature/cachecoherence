/* package */ enum BlockState {
    X ("X"),
    I ("I"),
    S ("S"),
    M ("M"),
    E ("E"),
    O ("O"),
    IM ("IM"),
    IS ("IS");
    
    private final String name;       

    private BlockState(String s) {
        name = s;
    }

    public String toString() {
       return this.name;
    }
}
