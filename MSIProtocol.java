//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/* package */ class MSIProtocol implements IProtocol {
	Logger log = LogManager.getLogger(Main.class.getName());

	private final static String name = "MSI";
	private CacheController cacheController = null;

	public MSIProtocol(CacheController cacheController) {
		this.cacheController = cacheController;
	}

	public String loadCacheRequest (Block block, int processorId) {
		log.debug("LOAD: (P"+processorId+" , "+block.getObjId()+") current state: "+block.getState());
		switch (block.getState()) {
			case I:
				log.trace("send LOAD-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				String value = cacheController.submitLoadSnoopRequest(block.getObjId());
				if (value == null) {
					value = cacheController.readFromMemory(block.getObjId());
					block.setValue(value);
				} else {
					block.setValue(value);
				}
				block.setState(BlockState.S);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Shared");
				break;
			case S :
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Shared to Shared");
				break;
			case M :
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Modified");
				break;
		}
		return block.getValue();
	}

	public void storeCacheRequest(Block block, String value, int processorId) {
		log.debug("STORE: (P"+processorId+" , "+block.getObjId()+","+value+") current state: "+block.getState());
		switch(block.getState()) {
			case I :
				log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Modified");
				break;
			case M : block.setValue(value);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Modified");
				break;
			case S :
				log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Shared to Modified");
				break;
		}
	}

	public String loadSnoopRequest (Block block, int processorId) {
		log.debug("process LOAD-SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		Block result = null;
		switch (block.getState()) {
			case I :
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid");
					break;
			case M :
					cacheController.writeToMemory(block);
					block.setState(BlockState.S);
					result = block;
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Shared");
					break;
			case S :
					result = block;
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Shared");
					break;
			default : throw new RuntimeException("Invalid load snoop request");
		}
		return (result == null ? null : result.getValue());
	}

	public void storeSnoopRequest (Block block, int processorId) {
		log.debug("process STORE SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid");
		block.setState(BlockState.I);
	}
	
	public void evict(Block blk, int procId) {
	   int objId = blk.getObjId();
	   BlockState state = blk.getState();
	   switch(state) {
		   case M:
			   cacheController.writeToMemory(blk);
			   blk.setState(BlockState.I);
			   break;
			case S:
			case I:
			   blk.setState(BlockState.I);
			   break;
			default:
			   throw new RuntimeException("Invalid Object State");
		}
   }

};
