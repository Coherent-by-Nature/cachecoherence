//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/* package */ class Processor {
	Logger log = LogManager.getLogger(Main.class.getName());

	private final static int CACHE_SIZE = 100;
	private CacheController cacheController;
	private int processorId;

	public Processor(ProtocolType protocolType, int processorId) {
		cacheController = new CacheController(CACHE_SIZE, protocolType, processorId);
		this.processorId = processorId;
		log.info("P" + processorId + ": Processor init");
	}

	public String load(int objId) {
		return cacheController.loadCacheRequest(objId, processorId);
	}

	public void store(int objId, String value) {
		cacheController.storeCacheRequest(objId, value, processorId);
	}
	
	public void flush() {
		cacheController.flush();
	}

	public void clear() {
		cacheController.clear();
	}

	public CacheController getCacheController() {
		return cacheController;
	}

	public int getProcessorId() {
		return processorId;
	}

	public void setProcessorId(int processorId) {
		processorId = processorId;
	}

	public int getCacheHits() {
		return getCacheController().getCacheHit();
	}

	public int getCacheMiss() {
		return getCacheController().getCacheMiss();
	}
	
	public String getState() {
		String res = " cacheHit: " + getCacheController().getCacheHit();
		res += " cacheMiss: " + getCacheController().getCacheMiss();
		return res;
	}
}
