class MemoryController {
	private static final MemoryController instance = new MemoryController();
	private Memory memory = null;
	private int readCount = -1;
	private int writeCount = -1;
	
	private MemoryController() {
		memory = new Memory();
		readCount = 0;
		writeCount = 0;
	}
	
	public static MemoryController getInstance() {
		return instance;
	}
	
	public String getObject(int objId) {
		readCount++;
		return memory.getObject(objId);
	}
	
	public void addObject(int objId, String value) {
		writeCount++;
		memory.addObject(objId, value);
	}
	
	public int getReadCount() {
		return readCount;
	}
	
	public int getWriteCount() {
		return writeCount;
	}
}
