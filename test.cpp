#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <cstdio>

using namespace std;

int main() {
	int n=110;
	srand(std::time(0));
	for(int i=0; i<n; i++) {
		cout<<"m "<<i<<" "<<rand()%100000<<endl;
	}
	for(int i=0; i<n; i++) {
		for(int j=0; j<5; j++) {
			int rn = rand()%2;
			int k = rand()%100;
			if(rn==0)
				cout<<"r "<<k<<endl;
			else
				cout<<"w "<<k<<" "<<rand()%100000<<endl;
		}
	}
	return 0;
}
