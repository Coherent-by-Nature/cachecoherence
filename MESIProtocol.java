import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/* package */ class MESIProtocol implements IProtocol {
	Logger log = LogManager.getLogger(Main.class.getName());

	private final static String name = "MESI";
	private CacheController cacheController = null;

	public MESIProtocol(CacheController cacheController) {
		this.cacheController = cacheController;
	}

	public String loadCacheRequest (Block block, int processorId) {
		log.debug("LOAD: (P"+processorId+" , "+block.getObjId()+") current state: "+block.getState());
		switch (block.getState()) {
			case I:
				log.trace("send LOAD-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				String value = cacheController.submitLoadSnoopRequest(block.getObjId());
				if (value == null) {
					value = cacheController.readFromMemory(block.getObjId());
					block.setValue(value);
					block.setState(BlockState.E);
					log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Exclusive");
				} else {
					block.setValue(value);
					block.setState(BlockState.S);
					log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Shared");
				}
				break;
			case M : 	log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Modified");
						break;
			case S : 	log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Shared to Shared");
						break;
			case E : 	log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Exclusive to Exclusive");
						break;
		}
		return block.getValue();
	}

	public void storeCacheRequest(Block block, String value, int processorId) {
		log.debug("STORE: (P"+processorId+" , "+block.getObjId()+","+value+") current state: "+block.getState());
		switch(block.getState()) {
			case I :
				log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Modified");
				break;
			case M :
				block.setValue(value);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Modified");
				break;
			case S :
				log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Shared to Modified");
				break;
			case E :
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Exclusive to Modified");
				break;
		}
	}

	public String loadSnoopRequest (Block block, int processorId) {
		log.debug("process LOAD-SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		Block result = null;
		switch (block.getState()) {
			case I :log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid"); 
					break;
			case M :
					cacheController.writeToMemory(block);
					block.setState(BlockState.S);
					result = block;
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Shared");
					break;
			case S :
					result = block;
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Shared");
					break;
			case E : 
					result = block;
					block.setState(BlockState.S);
					log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Shared");
					break;
			default : throw new RuntimeException("Invalid load snoop request");
		}
		return (result == null ? null : result.getValue());
	}

	public void storeSnoopRequest (Block block, int processorId) {
		log.debug("process STORE SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid");
		block.setState(BlockState.I);
	}

   public void evict(Block blk, int procId) {
	   int objId = blk.getObjId();
	   BlockState state = blk.getState();
	   switch(state) {
		   case M:
			   cacheController.writeToMemory(blk);
			   blk.setState(BlockState.I);
			   break;
			case E:
			case S:
			case I:
			   blk.setState(BlockState.I);
			   break;
			default:
			   throw new RuntimeException("Invalid Object State");
		}
   }


};
