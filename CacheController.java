//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;

/* package */ class CacheController {
	Logger log = LogManager.getLogger(Main.class.getName());
	private LRUCache cache;
	private IProtocol protocol = null;
	private int cacheHit = 0;
	private int cacheMiss = 0;
	private int processorId;

	public CacheController(int size, ProtocolType protocolType, int processorId) {
		Logger log = LogManager.getLogger(CacheController.class.getName());
		cache = new LRUCache(size);
		this.processorId = processorId;

		switch (protocolType) {
				case MSI : protocol = new MSIProtocol(this);
					log.info("P" + processorId + ": CacheController init");
					break;
				case MESI : protocol = new MESIProtocol(this);
					log.info("P" + processorId + ": CacheController init");
					break;
				case MOESI : protocol = new MOESIProtocol(this);
					log.info("P" + processorId + ": CacheController init");
					break;
				case MOSI : protocol = new MOSIProtocol(this);
					log.info("P" + processorId + ": CacheController init");
					break;
		}
	}
	
	public void flush() {
		Collection<Map.Entry<Integer,Block>> entries = cache.getAll();
		for (Map.Entry<Integer, Block> entry: entries) {
			Block block = entry.getValue();
			protocol.evict(block, processorId);
		}
	}

	public void clear() {
		cache.clear();
	}

	public String loadCacheRequest(int objId, int processorId) {
		Block block = cache.get(objId);
		if (block.getState() == BlockState.I) {
			cacheMiss++;
			log.error("P" + processorId + ": cache miss for object " + objId);
		} else {
			cacheHit++;
			log.debug("P" + processorId + ": cache hit for object " + objId + ", value: " + block.getValue());
		}
		return protocol.loadCacheRequest(block, processorId);
	}

	public void storeCacheRequest(int objId, String value, int processorId) {
		Block block = cache.get(objId);
		protocol.storeCacheRequest(block, value, processorId);
	}

	public String submitLoadSnoopRequest(int objId) {
		return Bus.getInstance().loadSnoopRequest(objId, processorId);
	}

	public String loadSnoopRequest(int objId) {
		Block block = cache.get(objId);
		return protocol.loadSnoopRequest(block, processorId);
	}

	public void submitStoreSnoopRequest(int objId) {
		Bus.getInstance().storeSnoopRequest(objId, processorId);
	}

	public void storeSnoopRequest(int objId) {
		Block block = cache.get(objId);
		protocol.storeSnoopRequest(block, processorId);
	}

	public void writeToMemory(Block block) {
		Bus.getInstance().writeToMemory(block.getObjId(), block.getValue());
	}

	public String readFromMemory(int objId) {
		return Bus.getInstance().readFromMemory(objId);
	}

	public int getCacheHit() {
		return cacheHit;
	}

	public int getCacheMiss() {
		return cacheMiss;
	}
}
