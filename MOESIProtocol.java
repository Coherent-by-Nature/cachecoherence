import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class MOESIProtocol implements IProtocol {
	Logger log = LogManager.getLogger(Main.class.getName());
	private final static String name = "MOESI";
	private CacheController cacheController = null;
	
	public MOESIProtocol(CacheController cacheController) {
		this.cacheController = cacheController;
	}

    public String loadCacheRequest(Block blk, int procId) {
		BlockState state = blk.getState();
		int objId = blk.getObjId();
		log.debug("LOAD: (P"+procId+" , "+objId+") current state: "+state);
        switch(state) {
            case M:
            case O:
            case E:
            case S:
				log.trace("(P"+procId+" , "+objId+") Transiton: No change");
                break;
            case I:
				log.trace("send LOAD-SNOOP: (P" + procId + " , " + objId+")");
				String val = cacheController.submitLoadSnoopRequest(objId);
				if (val == null) {
					val = cacheController.readFromMemory(objId);
					blk.setValue(val);
                    blk.setState(BlockState.E);
					log.trace("(P"+procId+" , "+objId+") Transiton: Invalid to Exclusive");
				}
				else {
					blk.setValue(val);
					blk.setState(BlockState.S);
					log.trace("(P"+procId+" , "+objId+") Transiton: Invalid to Shared");
				}
                break;
            default:
                throw new RuntimeException("Invalid Object State");
        }
        return blk.getValue();
    }    
    
    public void storeCacheRequest(Block blk,  String val,  int procId) {
        BlockState state = blk.getState();
	int objId = blk.getObjId();
	log.debug("STORE: (P"+procId+" , "+objId+","+val+") current state: "+state);
        switch(state) {
            case M:
		blk.setValue(val);
		log.trace("(P"+procId+" , "+objId+") Transiton: No change");
                break;
            case O:
		log.trace("send STORE-SNOOP: (P" + procId + " , " + objId+")");
		cacheController.submitStoreSnoopRequest(objId);
		blk.setValue(val);
                blk.setState(BlockState.M);
		log.trace("(P"+procId+" , "+objId+") Transiton: Owned to Modified");
                break;
            case E:
		blk.setValue(val);
                blk.setState(BlockState.M);
		log.trace("(P"+procId+" , "+objId+") Transiton: Exclusive to Modified");
                break;
            case S:
		log.trace("send STORE-SNOOP: (P" + procId + " , " + objId+")");
		cacheController.submitStoreSnoopRequest(objId);
		blk.setValue(val);
                blk.setState(BlockState.M);
		log.trace("(P"+procId+" , "+objId+") Transiton: Shared to Modified");
                break;
            case I:
		log.trace("send STORE-SNOOP: (P" + procId + " , " + objId+")");
		cacheController.submitStoreSnoopRequest(objId);
		blk.setValue(val);
                blk.setState(BlockState.M);
		log.trace("(P"+procId+" , "+objId+") Transiton: Invalid to Modified");
		break;
            default:
                throw new RuntimeException("Invalid Object State");
        }
    }    
    
   public String loadSnoopRequest(Block blk, int procId) {
	   int objId = blk.getObjId();
	   BlockState state = blk.getState();
	   Block result = null;
	   log.debug("process LOAD-SNOOP (P" + procId + "," + objId + ") current state: " + state);

	   switch(state) {
		case M:
			blk.setState(BlockState.O);
			result = blk;
			log.trace("(P"+procId+" , "+objId+") Mark Owned");
			break;
		case O:
			result = blk;
			log.trace("(P"+procId+" , "+objId+") No change");
			break;
		case E:
			blk.setState(BlockState.S);
			result = blk;
			log.trace("(P"+procId+" , "+objId+") Mark Shared");
			break;
		case S:
			result = blk;
			log.trace("(P"+procId+" , "+objId+") No Change");
			break;
		case I:
			log.trace("(P"+procId+" , "+objId+") No Change");
			break;
		default:
                	throw new RuntimeException("Invalid Object State");
	   }
	   return (result==null?null:result.getValue());
   }

   public void storeSnoopRequest(Block blk, int procId) {
	   int objId = blk.getObjId();
	   BlockState state = blk.getState();
	   log.debug("process STORE-SNOOP (P" + procId + "," + objId + ") current state: " + state);
	   log.trace("(P"+procId+" , "+objId+") Mark Invalid");
	   blk.setState(BlockState.I);
   }

   public void evict(Block blk, int procId) {
	   int objId = blk.getObjId();
	   BlockState state = blk.getState();
	   switch(state) {
		   case M:
		   case O:
			   cacheController.writeToMemory(blk);
			   blk.setState(BlockState.I);
			   break;
			case E:
			case S:
			case I:
			   blk.setState(BlockState.I);
			   break;
			default:
			   throw new RuntimeException("Invalid Object State");
		}
   }
};
