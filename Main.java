//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.apache.logging.log4j.PropertyConfigurator;
import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
		//String log4jConfPath = "./libs/log4j.properties";
		//PropertyConfigurator.configure(log4jConfPath);

		Logger log = LogManager.getLogger(Main.class.getName());
        int nProcessors = Integer.parseInt(args[0]);

        int type = Integer.parseInt(args[1]);
        ProtocolType protocolType = ProtocolType.values()[type];

        log.info("Start Simulator: #Processors: " + nProcessors + " and Protocol: " + protocolType);
        BufferedReader br = new BufferedReader(new FileReader(args[2]));
        Processor[] processors = new Processor[nProcessors];
        for (int i = 0; i < nProcessors; ++i) {
            processors[i] = new Processor(protocolType, i);
        }

        MemoryController memoryController = MemoryController.getInstance();

        Bus bus = Bus.getInstance();
        bus.setProcessors(processors);
        bus.setMemoryController(memoryController);

        String line = br.readLine();
        int processorNum = -1;
		System.out.println();
	int processorMessage = 0;
        while (line != null) {
            processorNum = (processorNum+1)%nProcessors;
            String[] request = line.split(" ");
            int objId = -1;
            String value = null;
            log.info("Execute:<" + line+ "> on P"+processorNum);
	    switch (request[0]) {
                case "m" :  objId = Integer.parseInt(request[1]);
                            value = request[2];
                            memoryController.addObject(objId,value);
                            break;
                case "r" :  objId = Integer.parseInt(request[1]);
                            value = processors[processorNum].load(objId);
                            log.info("Value read for object "+objId+" is "+value);
                            processorMessage = processorMessage + 1;
			                break;
                case "w" :  objId = Integer.parseInt(request[1]);
                            value = request[2];
                            log.trace("Value stored for object "+objId+" is "+ value);
                            processors[processorNum].store(objId, value);
			                processorMessage = processorMessage + 1;
                            break;
            }

	    System.out.println();
            line = br.readLine();
        }
        
        //Flushing and clear the cache at end
        for (int i = 0; i < nProcessors; ++i) {
			processors[i].flush();
            processors[i].clear();
		}

        log.info("Simulator finished");
	log.info("Result Summary");
	log.info("Processor\tCacheHit\tCacheMiss");
        int totalCacheHit = 0;
	int totalCacheMiss = 0;
	for (int i = 0; i < nProcessors; ++i) {
		int cacheHit = processors[i].getCacheHits();
		int cacheMiss = processors[i].getCacheMiss();
		log.info("P" + i +"\t\t"+ cacheHit+"\t\t"+cacheMiss);
		totalCacheHit += cacheHit;
		totalCacheMiss += cacheMiss;
	}	
	log.info("Total Cache Hit:" + totalCacheHit +"\tTotal Cache Miss:"+ totalCacheMiss);
	
	log.info("Memory Statistics:");
	log.info("Mem reads:"+ memoryController.getReadCount()+"\tMem writes:"+memoryController.getWriteCount());
	int totalMessages = Bus.getInstance().getMessageCount() + processorMessage;
 	log.info("Total number of messages:\t"+ totalMessages);        
        System.exit(0);
    }
}
