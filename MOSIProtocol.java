import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class MOSIProtocol implements IProtocol {
	
	Logger log = LogManager.getLogger(Main.class.getName());
	private final static String name = "MOSI";
	private CacheController cacheController = null;
	
	public MOSIProtocol(CacheController cacheController)
	{
		this.cacheController = cacheController;
	}
	
	public String loadCacheRequest(Block block, int processorId) {
		
		log.debug("LOAD: (P"+processorId+" , "+block.getObjId()+") current state: "+block.getState());
		switch(block.getState())
		{
			case M:
			case O:
			case S: log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: No State Transiton");
				break;
			case I: log.trace("send LOAD-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				String value = cacheController.submitLoadSnoopRequest(block.getObjId());
				if(value == null)
				{
					value = cacheController.readFromMemory(block.getObjId());
					block.setValue(value);
				}
				else 
				{
					block.setValue(value);
				}
				block.setState(BlockState.S);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Shared");
				break;
		}
		//return "hello";
		return block.getValue();
	}
	
	public void storeCacheRequest(Block block, String value, int processorId) {
		log.debug("STORE: (P"+processorId+" , "+block.getObjId()+","+value+") current state: "+block.getState());
		switch(block.getState())
		{
			case I: log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Invalid to Modified");
				break;
				
			case M: block.setValue(value);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Modified");
				break;
				
			case S: log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Shared to Modified");
				break;	
			
			case O: log.trace("send STORE-SNOOP: (P" + processorId + " , " + block.getObjId()+")");
				cacheController.submitStoreSnoopRequest(block.getObjId());
				block.setValue(value);
				block.setState(BlockState.M);
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Owned to Modified");
				break;
		}
		
	}
	
	public String loadSnoopRequest (Block block, int processorId) {
		log.debug("process LOAD-SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		Block result = null;
		switch(block.getState())
		{
			case I: log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid");
				break;
				
			case M: block.setState(BlockState.O);
				result = block;
				log.trace("(P"+processorId+" , "+block.getObjId()+") Transiton: Modified to Owned");
				break;
			
			case S: result = block;
				log.trace("(P"+processorId+" , "+block.getObjId()+") No Transition");
				break;
				
			case O: result = block;
				log.trace("(P"+processorId+" , "+block.getObjId()+") No Transition");
				break;
		}
		return (result==null?null:result.getValue());
	}
	
	public void storeSnoopRequest (Block block, int processorId) {
		log.debug("process STORE SNOOP (P" + processorId + "," + block.getObjId() + ") current state: " + block.getState());
		log.trace("(P"+processorId+" , "+block.getObjId()+") Mark Invalid");
		block.setState(BlockState.I);
	}
		
	public void evict(Block block, int processorId) {
		int objId = block.getObjId();
		BlockState state = block.getState();
		switch(state) {
			case M:
			case O: cacheController.writeToMemory(block);
					block.setState(BlockState.I);
					break;
			case S:
			case I:	block.setState(BlockState.I);
					break;
			default:
				throw new RuntimeException("Invalid Object State");
		}
	}
	
}