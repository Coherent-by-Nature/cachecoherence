public class Block {
	private BlockState state;
	private int objId;
	private String value;

	public Block(int objId, String value) {
		this.objId = objId;
		this.value = value;
		state = BlockState.I;
	}
	
	public int getObjId() {
		return objId;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public BlockState getState() {
		return state;
	}
	
	public void setState(BlockState state) {
		this.state = state;
	}

	public String toString() {
		String out = "[" + objId + ", " + value + ", " + state.toString() + "]";
		return out;
	}
}
