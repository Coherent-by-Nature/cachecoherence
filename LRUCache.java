import java.util.HashMap;
import java.util.Collection;
import java.util.Map;
import java.util.ArrayList;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/* package */ class LRUCache {
	Logger log = LogManager.getLogger(Main.class.getName());
	private HashMap<Integer,Block> map;
	private int cacheSize;
	
	public LRUCache(int size) {
		cacheSize = size;
		map = new HashMap<Integer,Block>(cacheSize);
	}

	public Block get(int objId) {
		if (map.containsKey(objId)) {
			Block block = map.get(objId);
			return block;
		} else {
			Block block = new Block(objId, "Invalid");
			map.put(objId, block);
			return block;
		}
	}

	public void put(int objId, Block block) {
		map.put(objId, block);
	}
	
	public void put(int objId, String value) {
		Block block = new Block(objId, value);
		map.put(objId, block);
	}

	public void remove(int objId) {
		map.remove(objId);
	}

	public int getCacheSize() {
		return map.size();
	}

	public Collection<Map.Entry<Integer,Block>> getAll() {
		return new ArrayList<Map.Entry<Integer,Block>>(map.entrySet());
	}
	
	public void clear() {
		map.clear();
	}
	
	public String toString() {
		StringBuffer outputBuffer = new StringBuffer("{");
		Collection<Map.Entry<Integer,Block>> entries = getAll();
		for (Map.Entry<Integer, Block> entry: entries) {
			outputBuffer.append(entry.getValue().toString());
		}
		outputBuffer.append("}");
		return outputBuffer.toString();
	}
}
