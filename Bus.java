//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * Singleton class to manage interaction between caches and memory.
 */

public class Bus {
    Logger log = LogManager.getLogger(Bus.class.getName());
    private static Bus instance = null;
    private MemoryController memoryController = null;
    Processor[] processors = null;
    private int messages=0; 
    
    private Bus() {
    }
    
    public static Bus getInstance() {
        if (instance == null) {
            instance = new Bus();
        }
        
        return instance;
    }
    
    public void setProcessors(Processor[] processors) {
        this.processors = processors;
    }
    
    public void setMemoryController(MemoryController memoryController) {
        this.memoryController = memoryController;
    }
    
    public String readFromMemory(int objId) {
        messages = messages + 1;
        return memoryController.getObject(objId);
    }
    
    public void writeToMemory(int objId, String value) {
        messages = messages + 1;
        memoryController.addObject(objId, value);
    }
    
    public String loadSnoopRequest(int objId, int processorId) {
		//log.info("P" + processorId + ": loadSnoopRequest for " + objId);
        String result = null;
        for (Processor processor : processors) {
            String temp = processor.getCacheController().loadSnoopRequest(objId);
	    messages = messages+1;
            if (result == null)
                result = temp;
	}
	//messages = messages + processors.length;
        return result;
    }
    
    public void storeSnoopRequest(int objId, int processorId) {
		//log.info("P" + processorId + ": storeSnoopRequest for " + objId);
        for (Processor processor : processors) {
            processor.getCacheController().storeSnoopRequest(objId);
	    messages = messages+1;
        }
        //messages = messages + processors.length;
    }
    
    public int getMessageCount()
    {
	return messages;
    }
}
