JFLAGS = -g 
JC = javac
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
        Block.java \
        BlockState.java \
        Bus.java \
	CacheController.java \
	IProtocol.java \
	LRUCache.java \
	MemoryController.java \
	Memory.java \
	MSIProtocol.java \
	MESIProtocol.java \
	MOESIProtocol.java \
	Processor.java \
	ProtocolType.java \
        Main.java 

default: classes

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
