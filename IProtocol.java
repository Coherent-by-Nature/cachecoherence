/* file name: IProtocol.java */
interface IProtocol {
    public String loadCacheRequest(Block block, int processorId);
    public void storeCacheRequest(Block block, String value, int processorId);
    public String loadSnoopRequest(Block block, int processorId);
    public void storeSnoopRequest(Block block, int processorId);
    public void evict(Block block, int processorId);
}
